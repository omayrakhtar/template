# -*- coding: utf-8 -*-
'''
Author         : [Author Name]    
Contact        : [Author Email]
Copyright      : Top Data Science Ltd.
Created        : [Date]
Latest Version : [Date]
Description    : settings and configurations
'''

from __future__ import absolute_import
from __future__ import print_function

__author__ = 'Top Data Science Ltd.'

from os.path import join
from os.path import abspath
from os.path import dirname
from os import pardir


class Config(object):

    # directory configs
    CURRENT_DIR = abspath(dirname(__file__))      # Up to source/
    ROOT_DIR = abspath(join(CURRENT_DIR, pardir)) # Up to [your project]/
    DATA_DIR = abspath(join(ROOT_DIR, "data")) # Up to data/
    MODELS_DIR = abspath(join(ROOT_DIR, "models")) # Up to models/
    LOGS_DIR = abspath(join(ROOT_DIR, "logs")) # Up to logs/

    # [Other project related global variable] 


