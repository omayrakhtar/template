[Your Project Name]
======================

[Brief project description.]

Quickstart
----------

Run the following commands to bootstrap your environment.

    git clone git@gitlab.com:[gitlab link to your project]
    cd [your project]

**Setting up python**
This project uses python 3.[version]. ``pip3`` should be installed. ``sudo`` might be used.

    cd source
    pip3 install -r requirements.txt

Directory Structure
----------

- **data**: All the data files are in [your project]/data. This folder is in .gitignore.
- **logs** : All log files are in [your project]/logs. This folder is in .gitignore.
- **models** : All model files are in [your project]/models. This folder is in .gitignore.
- **source** : Source code in python. It has several subdirectories:
	- *data_wrangling*
	- *exploratory_analysis*
	- *feature extraction*
	- *modeling*
	- *image_processing*
	- *notebooks*
	- *visualization*


Contacts
----------

**Top Data Science:**

  oguzhan.gencoglu@topdatascience.com
  
  [your email address]
  
  timo.heikkinen@topdatascience.com

